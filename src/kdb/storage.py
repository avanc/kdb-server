# Copyright (C) 2021 Sven Klomp (mail@klomp.eu)
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA  02110-1301, USA.

import logging
logger = logging.getLogger(__name__)

import pathlib
import re

class DiskStorage(object):
  def __init__(self, folderpath):
    self.folder=pathlib.Path(folderpath)
    if not self.folder.is_dir():
      raise NotADirectoryError
    
  def getPage(self, name):
    logger.debug("Get page " + name)
    try:
      content= (self.folder / (name + '.md')).read_text(encoding="utf8")
    except FileNotFoundError:
      content=None
    
    return content

  def getPagesList(self):
    pageslist=[]
    for currentFile in self.folder.glob("*.md"):
      pageslist.append({"name": currentFile.stem})
    return pageslist
  
  def writePage(self, name, content, overwrite=False):
    pagefile=self.folder / (name + '.md')
    if not overwrite:
      if pagefile.exists():
        return False
    pagefile.write_text(content, encoding="utf8")
    return True

  def deletePage(self, name):
    pagefile=self.folder / (name + '.md')
    if (pagefile.is_file()):
      pagefile.unlink()
      return True
    else:
      return False

  def getReferences(self, name):
    mylist = {}
    tagsearch="(?:\|([^\[\]\|]*))?"
    pattern = re.compile("\[\["+ name.replace("_", " ") + tagsearch + tagsearch + tagsearch + tagsearch + "\]\]")
    for currentFile in self.folder.glob("*.md"):
      with currentFile.open(encoding="utf8") as f:
        result=pattern.findall(f.read())
        for tags in result:
          tmplist={}
          for tag in tags:
            if tag != "":
              tmplist[tag]=True
          if (len(tmplist)==0):
            if not "no tag" in mylist:
              mylist["no tag"]=[]
            mylist["no tag"].append(currentFile.stem)
          else:
            for tag in tmplist:
              if not tag in mylist:
                mylist[tag]=[]
              mylist[tag].append(currentFile.stem)
            
    return mylist
