# Copyright (C) 2021 Sven Klomp (mail@klomp.eu)
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA  02110-1301, USA.


import argparse
import os.path

import logging
logger = logging.getLogger(__name__)

from .version import __version__



LOG_LEVELS = {  'debug':    logging.DEBUG,
                'info':     logging.INFO,
                'warning':  logging.WARNING,
                'error':    logging.ERROR,
                'critical': logging.CRITICAL}


parser = argparse.ArgumentParser(description='Knowledge Database')

parser.add_argument('--version',
                    action='version',
                    version='%(prog)s {0}'.format(__version__))

parser.add_argument('--log-level',
                    dest='loglevel',
                    metavar='debug',
                    default="info",
                    help='Log Level: debug, info, warning, error, critical')


def parse(args=None):
    if args is None:
        options = parser.parse_args()
    else:
        options = parser.parse_args(args)
            
    if options.loglevel not in LOG_LEVELS.keys():
        parser.error("Log level \"{0}\" unknown".format(options.loglevel))
    logging.basicConfig(level=logging.INFO)
    logging.getLogger("kdb").setLevel(LOG_LEVELS.get(options.loglevel, logging.NOTSET))
    logging.getLogger("KDB server").setLevel(LOG_LEVELS.get(options.loglevel, logging.NOTSET))
    
    return options
