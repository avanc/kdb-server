#! /usr/bin/env python

import logging
logging.basicConfig(level=logging.INFO)
logging.getLogger("kdb").setLevel(logging.DEBUG)

logger = logging.getLogger()

import unittest

import sys
sys.path.insert(1, '../src')
from kdb import storage
#from m2c import error


class TestFolder(unittest.TestCase):
  
  def test_missing_folder(self):
    self.assertRaises(NotADirectoryError, storage.DiskStorage, "/path/does/not/exists")

class TestDatabase(unittest.TestCase):
  
  def test_get_missing_page(self):
    mystorage=storage.DiskStorage("../testdata")
    self.assertIsNone(mystorage.getPage("notexisting"))

  def test_get_page(self):
    mystorage=storage.DiskStorage("../testdata")
    import pathlib
    content=pathlib.Path("../testdata/index.md").read_text()
    self.assertEqual(mystorage.getPage("index"), content)

  def test_get_page_withDot(self):
    mystorage=storage.DiskStorage("../testdata")
    import pathlib
    content=pathlib.Path("../testdata/with.suffix.md").read_text()
    self.assertEqual(mystorage.getPage("with.suffix"), content)

  def test_get_pagelist(self):
    mystorage=storage.DiskStorage("../testdata")
    self.assertCountEqual(mystorage.getPagesList(), [{"name": "index"}, {"name": "Page_1"}, {"name": "Page_2"}, {'name': 'with.suffix'}])


  def test_writePage(self):
    mystorage=storage.DiskStorage("../testdata")
    self.assertTrue(mystorage.writePage("Page_3", "Hello World"))
    import pathlib
    pagefile=pathlib.Path("../testdata/Page_3.md")
    content=pagefile.read_text()
    pagefile.unlink()
    self.assertEqual(content, "Hello World")

  def test_writePage_withDot(self):
    mystorage=storage.DiskStorage("../testdata")
    self.assertTrue(mystorage.writePage("Page_4.hello", "Hello World"))
    import pathlib
    pagefile=pathlib.Path("../testdata/Page_4.hello.md")
    content=pagefile.read_text()
    pagefile.unlink()
    self.assertEqual(content, "Hello World")

  def test_overwritePage(self):
    mystorage=storage.DiskStorage("../testdata")
    self.assertFalse(mystorage.writePage("Page_2", "Hello World"))
    import pathlib
    pagefile=pathlib.Path("../testdata/Page_2.md")
    content=pagefile.read_text()
    self.assertNotEqual(content, "Hello World")


  def test_deletePage(self):
    mystorage=storage.DiskStorage("../testdata")
    self.assertTrue(mystorage.writePage("Page_5", "Hello World"))
    self.assertTrue(mystorage.deletePage("Page_5"))
    import pathlib
    pagefile=pathlib.Path("../testdata/Page_5.md")
    self.assertFalse(pagefile.is_file())

  def test_deleteMissingPage(self):
    mystorage=storage.DiskStorage("../testdata")
    self.assertFalse(mystorage.deletePage("Page_5"))


  def test_references(self):
    mystorage=storage.DiskStorage("../testdata")
    references=mystorage.getReferences("index")
    expected={
      "no tag": ["Page_2", "Page_1"],
      "my tag": ["Page_2"],
      "additional tag": ["Page_2"]
      }
    self.assertDictEqual(references, expected)

if __name__ == '__main__':
  unittest.main()
