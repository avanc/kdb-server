# Copyright (C) 2021 Sven Klomp (mail@klomp.eu)
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA  02110-1301, USA.

import logging
logger = logging.getLogger("KDB server")

import tornado.ioloop
import tornado.web

DATA_DIR="data/"
import sys
sys.path.insert(1, './src')

from kdb import storage
from kdb import parameter

mystorage=storage.DiskStorage(DATA_DIR)

class MainHandler(tornado.web.RequestHandler):
    def get(self):
      with open('static/index.html') as f:
        self.write(f.read())

class PageHandler(tornado.web.RequestHandler):
    def set_default_headers(self):
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Headers", "x-requested-with")
        self.set_header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT, DELETE')
  
  
    def get(self, item):
      content=mystorage.getPage(item)
      if content:
        self.set_header("Content-Type", "text/plain")
        self.write(content)
      else:
        raise tornado.web.HTTPError(404)

    def put(self, item):
      mystorage.writePage(item, self.request.body.decode('utf-8'), overwrite=True)


    def delete(self, item):
      if (mystorage.deletePage(item)):
        self.set_status(200)
      else:
        self.set_status(404)
      
      self.finish()

    def options(self, item):
      # CORS preflight https://stackoverflow.com/a/35259440
      self.set_status(204)
      self.finish()

class ReferencesHandler(tornado.web.RequestHandler):
    def set_default_headers(self):
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Headers", "x-requested-with")
        self.set_header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT')
  
    def get(self, item):
      content=mystorage.getReferences(item)
      self.set_header("Content-Type", "text/plain")
      self.write(content)

    def options(self, item):
      # CORS preflight https://stackoverflow.com/a/35259440
      self.set_status(204)
      self.finish()



class ItemsHandler(tornado.web.RequestHandler):
    def set_default_headers(self):
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Headers", "x-requested-with")
        self.set_header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT')
  
  
    def get(self):
      items=mystorage.getPagesList()
      self.set_header("Content-Type", "application/json")
      self.write({"pages": items})

    def options(self):
      # CORS preflight https://stackoverflow.com/a/35259440
      self.set_status(204)
      self.finish()


def make_app():
    return tornado.web.Application([
    (r"/css/(.*)", tornado.web.StaticFileHandler, {"path": "static/css"}),
    (r"/js/(.*)", tornado.web.StaticFileHandler, {"path": "static/js"}),
    (r"/items/([^/]*)/references", ReferencesHandler),
    (r"/items/([^/]*)/?", PageHandler),
    (r"/items", ItemsHandler),
    (r"/[^/]*", MainHandler),
    ])

if __name__ == "__main__":
    options = parameter.parse()
    #configdata=config.readConfig(options.configfile)
    app = make_app()
    app.listen(8888)
    logger.info("Listening on http://localhost:8888")
    tornado.ioloop.IOLoop.current().start()
